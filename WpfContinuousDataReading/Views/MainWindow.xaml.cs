﻿using System.Windows;
using WpfContinuousDataReading.ViewModels;

namespace WpfContinuousDataReading.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            DataContext = new MainWindowViewModel();
            InitializeComponent();
        }
    }
}
