﻿using System.Threading.Tasks;
using WpfContinuousDataReading.Model;

namespace WpfContinuousDataReading.Services.Interfaces
{
    public interface IWeatherService
    {
        Task<Weather> GetWeatherAsync();
        void ProcessWeather(Weather weather);
    }
}
