﻿using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using WpfContinuousDataReading.Model;
using WpfContinuousDataReading.Services.Interfaces;

namespace WpfContinuousDataReading.Services
{
    public class WeatherService : IWeatherService
    {
        private const string WeatherApiUrl = "http://api.openweathermap.org/data/2.5/weather?lat=51.75&lon=19.4667&units=metric&appid=0a0bc0919e53954be8e20715e561ffb0";

        public async Task<Weather> GetWeatherAsync()
        {
            using (HttpClient client = new HttpClient())
            {
                var result = await client.GetStringAsync(WeatherApiUrl);
                var weather = JObject.Parse(result)["main"].ToObject<Weather>();
                weather.Time = DateTime.Now;
                return weather;
            }
        }

        public void ProcessWeather(Weather weather)
        {
            var randGen = new Random();
            var randomFactor = randGen.NextDouble() * 0.2 - 0.1;
            weather.Temp = Math.Round(weather.Temp + weather.Temp * randomFactor, 2);
            weather.Pressure = Math.Round(weather.Pressure + weather.Pressure * randomFactor, 2);
            weather.Humidity = Math.Round(weather.Humidity + weather.Humidity * randomFactor, 2);
        }
    }
}
