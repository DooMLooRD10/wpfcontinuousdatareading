﻿using System;
using System.Windows.Input;

namespace WpfContinuousDataReading.ViewModels.Base
{
    public class RelayCommand : ICommand
    {
        private Action _action;

        public RelayCommand(Action mAction)
        {
            _action = mAction;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _action();
        }

        public event EventHandler CanExecuteChanged;
    }
}
