﻿using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using WpfContinuousDataReading.Model;
using WpfContinuousDataReading.Services;
using WpfContinuousDataReading.Services.Interfaces;
using WpfContinuousDataReading.ViewModels.Base;

namespace WpfContinuousDataReading.ViewModels
{
    public class MainWindowViewModel : BaseViewModel
    {
        #region Properties

        public string TestText { get; set; }
        public int RefreshTime { get; set; }

        public bool IsRunning { get; set; }

        public SeriesCollection ChartSeries { get; set; }
        public ChartValues<DateTimePoint> ChartValues { get; set; }
        public Func<double, string> YFormatter { get; set; }
        public Func<double, string> XFormatter { get; set; }

        public ObservableCollection<Weather> WeatherCollection { get; set; }

        #endregion

        #region Fields

        private CancellationTokenSource _cancellationTokenSource;
        private readonly IWeatherService _weatherService;

        #endregion

        #region Commands

        public ICommand StartCommand { get; set; }
        public ICommand StopCommand { get; set; }

        #endregion

        #region Constructor

        public MainWindowViewModel()
        {
            _weatherService = new WeatherService();
            StartCommand = new RelayCommand(Start);
            StopCommand = new RelayCommand(Cancel);

            WeatherCollection = new ObservableCollection<Weather>();
            RefreshTime = 2;
            XFormatter = val => new DateTime((long)val).ToString("HH:mm:ss");
            YFormatter = val => val.ToString("N") + " °C";
            InitChartSeries();
        }

        #endregion

        #region Private Methods

        private void Start()
        {
            _cancellationTokenSource = new CancellationTokenSource();
            var cancellationToken = _cancellationTokenSource.Token;

            Task.Run(() => RunServer(cancellationToken), _cancellationTokenSource.Token);
        }

        private void Cancel()
        {
            _cancellationTokenSource.Cancel();
        }

        private void RunServer(CancellationToken cancellationToken)
        {
            IsRunning = true;
            while (true)
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    IsRunning = false;
                    break;
                }

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(async () =>
                {
                    var weather = await _weatherService.GetWeatherAsync();
                    _weatherService.ProcessWeather(weather);
                    WeatherCollection.Add(weather);
                    AddPointToChart(weather);
                }));

                cancellationToken.WaitHandle.WaitOne(RefreshTime * 1000);
            }
        }

        private void AddPointToChart(Weather weather)
        {
            ChartValues.Add(new DateTimePoint(weather.Time, weather.Temp));
            ChartValues.RemoveAt(0);
        }

        private void InitChartSeries()
        {
            ChartSeries = new SeriesCollection();
            ChartValues = new ChartValues<DateTimePoint>();

            for (int i = 20; i >= 0; i--)
            {
                ChartValues.Add(new DateTimePoint(DateTime.Now.AddSeconds(-RefreshTime * i), 0));
            }

            ChartSeries.Add(new LineSeries
            {
                Values = ChartValues,
                LineSmoothness = 0
            });
        }

        #endregion
    }
}
